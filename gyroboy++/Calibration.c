#include "ev3api.h"
#include "app.h"
#include "Util.h"
#include "ColorManager.h"
#include "Calibration.h"
#include "IniFileManager.h"
#include <string.h>

//colorStr Max:9
void SetColor(int *colorValue, char* colorStr, int y)
{
	char str[10] = {0};
	char cor[10] = {0};

	while(ev3_touch_sensor_is_pressed(EV3_PORT_2) == 0){

		*colorValue = Convert(ev3_color_sensor_get_reflect(EV3_PORT_3));
		
		sprintf(str,"[%s]=[%d]", colorStr, *colorValue);
		ev3_lcd_draw_string("         ", 1, y);
		ev3_lcd_draw_string(str, 1, y);
		
		// 電池残量を取得
		int battery = ev3_battery_voltage_mV();
		
		// 出力文字列生成
		sprintf(cor,"Battery=[%d]", battery);
		
		// 液晶に表示
		ev3_lcd_draw_string("         ", 1, 1);
		ev3_lcd_draw_string(cor, 1, 1);
				
		tslp_tsk(10);
	}

	SoundBeep();
	tslp_tsk(1000);
}

/* キャリブレーション */
void Calibration()
{
	int black, white, gray = 0;
	char str[10] = {0};
	
	// 黒色を設定する
	SetColor(&black, "black", 10);
	SetBlack(black);
	
	// 白色を設定する
	SetColor(&white, "white", 20);
	SetWhite(white);
	
	float black_gain = GetIniFileValue("BLACK_GAIN");
	float white_gain = GetIniFileValue("WHITE_GAIN");
	
	// ターゲット値を設定する
	SetRunTarget(((GetBlack() * black_gain) + (GetWhite() * white_gain)) / (black_gain + white_gain));

	sprintf(str,"[runTarget]=[%d]", GetRunTarget());
	ev3_lcd_draw_string("         ", 1, 30);
	ev3_lcd_draw_string(str, 1, 30);
	tslp_tsk(10);
	
	// 灰色を設定する
	SetColor(&gray, "gray", 40);
	SetGray(gray);

}

/* キャリブレーション ダミー実施 */
void DummyCalibration()
{
	int black     = GetIniFileValue("BLACK_VALUE");
	int white     = GetIniFileValue("WHITE_VALUE");
	int gray      = GetIniFileValue("GRAY_VALUE");
	int runTarget = GetIniFileValue("RUN_VALUE");
	
	// 値を設定する
	SetBlack(black);
	SetWhite(white);
	SetRunTarget(runTarget);
	SetGray(gray);
	
	// 値を表示する
	DispColorValue(black, "black", 10);
	DispColorValue(white, "white", 20);
	DispColorValue(gray, "gray", 30);
	DispColorValue(runTarget, "runTarget", 40);

}


/* カラー値 表示 */
void DispColorValue(int colorValue, char* colorStr, int y)
{
	char str[20] = {0};
	char cor[10] = {0};
	
	sprintf(str,"[%s]=[%d]", colorStr, colorValue);
	ev3_lcd_draw_string("         ", 1, y);
	ev3_lcd_draw_string(str, 1, y);
	
	// 電池残量を取得
	int battery = ev3_battery_voltage_mV();
	
	// 出力文字列生成
	sprintf(cor,"Battery=[%d]", battery);
	
	// 液晶に表示
	ev3_lcd_draw_string("         ", 1, 1);
	ev3_lcd_draw_string(cor, 1, 1);
			
	tslp_tsk(10);
}
