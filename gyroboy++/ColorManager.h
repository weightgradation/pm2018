#ifndef __COLOR_MANAGER__
#define __COLOR_MANAGER__

#include "PM2017.h"

int GetBlack();
void SetBlack(int setValue);

int GetWhite();
void SetWhite(int setValue);

int GetGray();
void SetGray(int setValue);

int GetRunTarget();
void SetRunTarget(int setValue);


void SetColorSampling(int setColor);
bool IsColorThreshold(int checkColor);
bool IsSamplengColorThreshold(int checkColor);

#endif	//__COLOR_MANAGER__

