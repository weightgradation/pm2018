#include "ev3api.h"
#include "app.h"
#include "RightLineTrace.h"
#include "ColorManager.h"
#include "PM2017.h"
#include "DataLogging.h"
#include "IniFileManager.h"
#include "Util.h"
#include "CalcLocation.h"

extern const int left_motor;
extern const int right_motor;

#define SPEED_DOWN_VALUE			(10)
#define X_POINT_START				(100)
#define X_POINT_END					(120)
#define Y_POINT_START				(20)
#define Y_POINT_END					(40)

int CheckModeTransitionRightLineTrace()
{
		// 暫定処理
	/*
	int x = (int)GetLocationX();
	int y = (int)GetLocationY();
	
	// x軸が100〜120, y軸が20〜40の場合
	if ((X_POINT_START < x && x < X_POINT_END) && 
		(Y_POINT_START < y && y < Y_POINT_END)) {
		return RUN_MODE_LEFT_CURVE;
	} else {
		return RUN_MODE_RIGHT_LINETRACE;
	}
	*/
	return RUN_MODE_RIGHT_LINETRACE;
	//return RUN_MODE_RIGHT_LINETRACE;
}

void ExecRightLineTrace(int light, int previousLight, int* steer)
{
	int black = 100; //GetBlack();
	int white = 0;   //GetWhite();
	int runTarget = GetRunTarget();
	

	float p_gain = GetIniFileValue("P_GAIN");
	float d_gain = GetIniFileValue("D_GAIN");
	int speed = (int)GetIniFileValue("DEFAULT_SPEED");
	
	// PID制御による旋回値取得
	float turn = p_gain * (light - runTarget) * 100 / (black - white) + d_gain * (light - previousLight) / (black - white) * 100;

	if(0 < light - runTarget)	// 黒に近い
	{
		// タイヤの逆走は行わない
		if(speed/2 < turn)
		{
			turn = speed/2;
		}

	*steer = speed;

		// 左を強く
		// ev3_motor_set_power(EV3_PORT_A,speed);
		// ev3_motor_set_power(EV3_PORT_D,speed - ((speed * turn)/100 * 2));


/*
		// 現在のカラーセンサーの値が黒色の値 - 10より
		if (light > black - 20) {
			// 左を強く
			ev3_motor_set_power(EV3_PORT_B,speed - SPEED_DOWN_VALUE);
			ev3_motor_set_power(EV3_PORT_C,speed - SPEED_DOWN_VALUE - (turn * 2));
		} else {
			// 左を強く
			ev3_motor_set_power(EV3_PORT_B,speed);
			ev3_motor_set_power(EV3_PORT_C,speed - (turn * 2));
		}
*/
	}
	else						// 白に近い
	{
		// タイヤの逆走は行わない
		if(turn <= speed/-2)
		{
			turn = speed/-2;
		}

		*steer = speed;
// 		// 右を強く
// 		ev3_motor_set_power(EV3_PORT_A, speed + ((speed * turn)/100 * 2));
// 		ev3_motor_set_power(EV3_PORT_D, speed);
// /*
		// if (light < white + 20) {
		// 	// 右を強く
		// 	ev3_motor_set_power(EV3_PORT_B, speed - SPEED_DOWN_VALUE + (turn * 2));
		// 	ev3_motor_set_power(EV3_PORT_C, speed - SPEED_DOWN_VALUE);
			
		// } else {
		// 	// 右を強く
		// 	ev3_motor_set_power(EV3_PORT_B, speed + (turn * 2));
		// 	ev3_motor_set_power(EV3_PORT_C, speed);
		// }
	//*/
	}

	// データロギング処理
	//WriteData(turn, light, black, white, runTarget);
	
}