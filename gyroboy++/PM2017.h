#ifndef __PM2017__
#define __PM2017__


/**********************************************
		定義値
**********************************************/

#define RETURN_OK	( 0)
#define RETURN_NG	(-1)

#define bool	int

//--------------------------------------------
//		走行モード
//--------------------------------------------
#define RUN_MODE_LEFT_LINETRACE			(1)
#define RUN_MODE_RIGHT_LINETRACE		(2)
#define RUN_MODE_CHAGNE_EDGE_TO_RIGHT	(3)
#define RUN_MODE_RIGHT_FIND_CROSS		(4)
#define RUN_MODE_RIGHT_CROSS			(5)
#define RUN_MODE_LEFT_FIND_CROSS		(6)
#define RUN_MODE_LEFT_CROSS				(7)
#define RUN_MODE_SHORTCUT_1				(8)
#define RUN_MODE_SHORTCUT_2				(9)
#define RUN_MODE_FIND_GRAY				(10)
#define RUN_MODE_LEFT_CURVE				(11)
#define RUN_MODE_CHAGNE_EDGE_TO_LEFT	(12)


/**********************************************
		関数
**********************************************/
void PM2017Main();
void SetDeviceConfig();
void Exit();
void CheckModeTransition();
void ExecRun(int light, int previousLight);
void PM2017Init();
void Standby();

#endif	//__PM2017__

