#ifndef __CALC_LOCATION__
#define __CALC_LOCATION__

void CalcLocation();
float GetLocationX();
float GetLocationY();

void OpenLocationFile();
void CloseLocationFile();
void WriteLocationData(float distanceRight, 
					   float distanceLeft, 
					   float distance4msRight, 
					   float distance4msLeft, 
					   float distance4ms, 
					   float diffdistance4ms, 
					   float radian, 
					   float calcRadian, 
					   float X, 
					   float Y);


void InitCalcLocation();

#endif	//__CALC_LOCATION__

