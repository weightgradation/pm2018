#include "ev3api.h"
#include "app.h"
#include "IniFileManager.h"
#include "PM2017.h"
#include "Util.h"
#include <stdlib.h>
#include <string.h>

#define KEY_LENGTH				(20)
#define INIFILE_VALUE_NUM		(30)

struct IniFileData
{
	char key[KEY_LENGTH];
	float  value;

};

struct IniFileData list[INIFILE_VALUE_NUM];

//******************************************
// 関数名   ：ExportFileData
// 関数概要 ：ファイルデータ書き出し処理
// 引数     ：無し
// 戻り値    ：無し
//******************************************
void ExportFileData(FILE* fp)
{
	char str[100];
	char* splitStr[2];
	int i = 0;
	
    // ファイルの終端まで文字を読み取る
    while ( fgets(str, sizeof(str), fp) != NULL ) 
	{
    	if(str[0] != '#' && str[0] != '\n')
    	{
	    	split(str, ",", splitStr);
			strcpy(list[i].key, splitStr[0]);
	    	list[i].value =  atof(splitStr[1]);

	    	i++;
    	}
    }
}


//******************************************
// 関数名   ：GetIniFileValue
// 関数概要 ：設定ファイル値取得処理
// 引数     ：設定値キー名称
// 戻り値    ：キー名称が存在した場合、キー名称に対応する値
// 戻り値    ：キー名称が存在しない場合、0
//******************************************
float GetIniFileValue(char* keyStr)
{
	for(int i = 0; i < INIFILE_VALUE_NUM; i++)
	{
		if(strcmp(list[i].key, keyStr) == 0)
		{
			return list[i].value;
		}
	}
	
	return 0;
}

//******************************************
// 関数名   ：ReadIniFileValue
// 関数概要 ：設定ファイル値読込処理
// 引数     ：無し
// 戻り値    ：正常終了(RETURN_OK)
// 戻り値    ：異常終了(RETURN_NG)
//******************************************
int ReadIniFileValue()
{
	FILE *iniFile;

	// 設定ファイルをオープンする
	if((iniFile = fopen("PM2017.ini", "r")) == NULL)
	{
		return RETURN_NG;
	}
	
	ExportFileData(iniFile);
	
	// ファイルクローズ
    fclose(iniFile);

	return RETURN_OK;
}



