#include "ev3api.h"
#include "app.h"
#include <string.h>

void SoundBeep(){
	
	ev3_speaker_play_tone(NOTE_C4, 100);
	ev3_sta_cyc(20);
	
	ev3_speaker_play_tone(NOTE_C4, 100);
	ev3_sta_cyc(50);
}


/* カラーセンサー取得値を変換 */
int Convert(int light) {
	
	int convert = 680 - 2 * (light-6);
	
	return convert;
}



int split( char *str, const char *delim, char *outlist[]) 
{
    char    *tk;
    int     cnt = 0;

    tk = (char*)strtok(str, delim);
    while( tk != NULL) 
	{
        outlist[cnt++] = tk;
		tk = (char*)strtok( NULL, delim );
    }
	
    return cnt;
}
