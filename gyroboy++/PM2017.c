#include "ev3api.h"
#include "app.h"
#include "PM2017.h"
#include "Util.h"
#include "Calibration.h"
#include "ColorManager.h"
#include "IniFileManager.h"
// #include "CalcLocation.h"
#include "DataLogging.h"
#include "LeftLineTrace.h"
#include "RightLineTrace.h"
#include "ChangeEdgeToRight.h"
#include "Distance.h"
#include "FindGrayRight.h"
#include "RightCross.h"
#include "LeftCurveLineTrace.h"
#include "ChangeEdgeToLeft.h"


const int touch_sensor = EV3_PORT_2;
const int color_sensor = EV3_PORT_3;
const int left_motor   = EV3_PORT_B;
const int right_motor  = EV3_PORT_C;

int runMode;


//******************************************
// 関数名   ：PM2017Main
// 関数概要 ：PM2017年用メイン関数
// 引数     ：無し
// 戻り値    ：無し
//******************************************
void PM2017Main()
{
	// PM2017年処理の初期化処理を行う
	PM2017Init();

	if((int)GetIniFileValue("CALIBRATION_FLAG") == 1)
	{
		// キャリブレーションを実施する
	    Calibration();
	}
	else
	{
		// キャリブレーション結果を表示する
		DummyCalibration();
	}

	// 起動時のみ前回カラーセンサーを設定する
	int previousLight = Convert((int)ev3_color_sensor_get_reflect(color_sensor));
	int light = previousLight;

	// 待機処理
	Standby();
	
    while (ev3_touch_sensor_is_pressed(EV3_PORT_2) == 0) S
	{
    	// 最新カラーセンサー値の取得、前回カラーセンサー値の更新
    	previousLight = light;
		light = Convert((int)ev3_color_sensor_get_reflect(color_sensor));
		
		// カラーセンサー値サンプリング処理
		SetColorSampling(light);
		
		// 走行距離更新処理
		UpdateDistance();
		
		// 自己位置算出処理
		CalcLocation();
		
		// モード遷移判定
		CheckModeTransition();
		
		// 走行処理を実施する
		ExecRun(light, previousLight);
		
		// スリープ処理
        tslp_tsk(4);
    }
    
	// 終了処理
	Exit();
}

//******************************************
// 関数名   ：PM2017Init
// 関数概要 ：PM2017年初期化処理
// 引数     ：無し
// 戻り値    ：無し
//******************************************
void PM2017Init()
{
	// デバイスの設定を行う
	SetDeviceConfig();

	// データロギングファイルを作成する
	OpenFile();

	// 自己位置算出用ファイルを作成する
	OpenLocationFile();

	// 設定ファイル値読込処理
	ReadIniFileValue();

	// 自己位置算出初期処理
	InitCalcLocation();

	// モーターの角度を初期化する
	InitDistance();

	// スタート時のモードを設定する
	runMode = (int)GetIniFileValue("START_MODE");
}


//******************************************
// 関数名   ：CheckModeTransition
// 関数概要 ：次モード遷移判定処理
// 引数     ：無し
// 戻り値    ：無し
//******************************************
void CheckModeTransition()
{
	switch(runMode)
	{
	case RUN_MODE_LEFT_LINETRACE:	// 左ライントレースモード
		runMode = CheckModeTransitionLeftLineTrace();
		break;
		
	case RUN_MODE_RIGHT_LINETRACE:	// 右ライントレースモード
		runMode = CheckModeTransitionRightLineTrace();
		break;
		
	case RUN_MODE_CHAGNE_EDGE_TO_RIGHT:		// エッジ切り替えモード（左→右）
		runMode = CheckModeTransitionChangeEdgeToRight();
		break;
	case RUN_MODE_RIGHT_FIND_CROSS:	//右クロス
	case RUN_MODE_RIGHT_CROSS:		// 右クロス
		runMode = CheckModeTransitionRightCross();
		break;
	case RUN_MODE_FIND_GRAY:	//灰色検知
		runMode = CheckModeTransitionFindGrayRight(RUN_MODE_FIND_GRAY, 99);
		break;
	case RUN_MODE_LEFT_CURVE:	// 左カーブ
		runMode = CheckModeTransitionLeftCurveLineTrace();
		break;
		case RUN_MODE_CHAGNE_EDGE_TO_LEFT:	//エッジ切替モード (右から左)
		runMode = CheckModeTransitionChangeEdgeToLeft();
		break;
	default:
		break;
	}
}

//******************************************
// 関数名   ：CheckModeTransition
// 関数概要 ：モード走行処理
// 引数     ：light 		   最新カラーセンサー値
// 引数     ：previousLight 前回カラーセンサー値
// 戻り値    ：無し
//******************************************
void ExecRun(int light, int previousLight)
{
	switch(runMode)
	{
	case RUN_MODE_LEFT_LINETRACE:	// 左ライントレースモード
		ExecLeftLineTrace(light, previousLight);
		break;
		
	case RUN_MODE_RIGHT_LINETRACE:	// 右ライントレースモード
		ExecRightLineTrace(light, previousLight);
		break;
		
	case RUN_MODE_CHAGNE_EDGE_TO_RIGHT:		// エッジ切り替えモード（左→右）
		ExecChangeEdgeToRight();
		break;
	case RUN_MODE_RIGHT_FIND_CROSS:	//右クロス
	case RUN_MODE_RIGHT_CROSS:		// 右クロス
		ExecRightCross(light, previousLight);
		break;
	case RUN_MODE_FIND_GRAY:		// エッジ切り替えモード（左→右）
		ExecFindGrayRight(light, previousLight);
		break;
	case 99:
		Exit();
		break;
	case RUN_MODE_LEFT_CURVE:	// 右ライントレースモード
		ExecLeftCurveLineTrace(light, previousLight);
		break;
	case RUN_MODE_CHAGNE_EDGE_TO_LEFT://エッジ切替モード (右から左)
		ExecChangeEdgeToLeft();
		break;
	default:
		break;
	}
}

//******************************************
// 関数名   ：SetDeviceConfig
// 関数概要 ：デバイス設定処理
// 引数     ：無し
// 戻り値    ：無し
//******************************************
void SetDeviceConfig()
{
    // Configure motors
    ev3_motor_config(left_motor, LARGE_MOTOR);
    ev3_motor_config(right_motor, LARGE_MOTOR);
	
	// モーター値の初期化を行う

    // Configure sensors
    ev3_sensor_config(touch_sensor, TOUCH_SENSOR);
    ev3_sensor_config(color_sensor, COLOR_SENSOR);
}


//******************************************
// 関数名   ：Exit
// 関数概要 ：終了処理
// 引数     ：無し
// 戻り値    ：無し
//******************************************
void Exit()
{
	// 左右のモーターを停止する
	ev3_motor_set_power(EV3_PORT_C,0);
	ev3_motor_set_power(EV3_PORT_B,0);
	
	// データロギングファイルをクローズする
	CloseFile();

	// 自己位置算出用ファイルをクローズする
	CloseLocationFile();
}

//******************************************
// 関数名   ：Standby
// 関数概要 ：待機処理
// 引数     ：無し
// 戻り値   ：無し
//******************************************
void Standby()
{
	// ボタンが押されるまで待機してる
	while(ev3_touch_sensor_is_pressed(EV3_PORT_2) == 0)
	{
		// 液晶に表示
		ev3_lcd_draw_string("           ",1,60);
		ev3_lcd_draw_string("PUSH START",1,60);
		tslp_tsk(10);
	}
	ev3_lcd_draw_string("           ",1,60);
	SoundBeep();
}
