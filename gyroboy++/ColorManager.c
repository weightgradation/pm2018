#include "ev3api.h"
#include "app.h"
#include "ColorManager.h"

#define SAMPLING_ELEMENT_NUM	(10)
#define THRESHOLD_VALUE			(15)

// 黒
int black;
// 白
int white;
// 灰
int gray;
// 走行目標値
int runTarget;
// カラーサンプリング配列
int samplengArray[SAMPLING_ELEMENT_NUM];
// カラーサンプリング配列設定位置
int colorIndex = 0;

//******************************************
// 関数名   ：GetBlack
// 関数概要 ：黒の値を返却する
// 引数     ：無し
// 戻り値    ：黒のカラーセンサー値
//******************************************
int GetBlack()
{
	return black;
}

//******************************************
// 関数名   ：SetBlack
// 関数概要 ：黒の値を更新する
// 引数     ：設定する黒のカラーセンサー値
// 戻り値    ：無し
//******************************************
void SetBlack(int setValue)
{
	black = setValue;
}

//******************************************
// 関数名   ：GetWhite
// 関数概要 ：白の値を返却する
// 引数     ：無し
// 戻り値    ：白のカラーセンサー値
//******************************************
int GetWhite()
{
	return white;
}

//******************************************
// 関数名   ：SetWhite
// 関数概要 ：白の値を更新する
// 引数     ：設定する白のカラーセンサー値
// 戻り値    ：無し
//******************************************
void SetWhite(int setValue)
{
	white = setValue;
}

//******************************************
// 関数名   ：GetGray
// 関数概要 ：灰色の値を返却する
// 引数     ：無し
// 戻り値    ：灰のカラーセンサー値
//******************************************
int GetGray()
{
	return gray;
}

//******************************************
// 関数名   ：SetGray
// 関数概要 ：灰の値を更新する
// 引数     ：設定する灰のカラーセンサー値
// 戻り値    ：無し
//******************************************
void SetGray(int setValue)
{
	gray = setValue;
}

//******************************************
// 関数名   ：GetRunTarget
// 関数概要 ：走行目標値の値を返却する
// 引数     ：無し
// 戻り値    ：走行目標値のカラーセンサー値
//******************************************
int GetRunTarget()
{
	return runTarget;
}

//******************************************
// 関数名   ：SetRunTarget
// 関数概要 ：走行目標値の値を更新する
// 引数     ：設定する走行目標値のカラーセンサー値
// 戻り値    ：無し
//******************************************
void SetRunTarget(int setValue)
{
	runTarget = setValue;
}


//******************************************
// 関数名   ：SetColorSampling
// 関数概要 ：最新カラーセンサー値をサンプリング配列に設定する
// 引数     ：最新カラーセンサー値
// 戻り値    ：無し
//******************************************
void SetColorSampling(int setColor)
{
	// 次の位置へ進める。(最大値を越えた場合は0に戻る)
	colorIndex++;
	if(colorIndex == SAMPLING_ELEMENT_NUM)
	{
		colorIndex = 0;
	}
	colorIndex %= SAMPLING_ELEMENT_NUM;

	// 現在の値を更新する
	samplengArray[colorIndex] = setColor;
	
}

//******************************************
// 関数名   ：IsColorThreshold
// 関数概要 ：サンプリングの平均値が
//          ：引数で指定された色の閾値内か判定する
// 引数     ：判定したいカラー（白、黒、灰、走行目標値）
// 戻り値    ：true（閾値内）  false(閾値外)
//******************************************
bool IsColorThreshold(int checkColor)
{
	int sum = 0;
	int ave = 0;
	
	// 配列の要素分だけ値を合算する
	for(int i = 0; i < SAMPLING_ELEMENT_NUM; i++){
		sum += samplengArray[i];
	}
	
	// 平均値を算出する
	ave = sum / SAMPLING_ELEMENT_NUM;
	
	// 閾値判定を実施する
	if(checkColor - THRESHOLD_VALUE < ave && ave < checkColor + THRESHOLD_VALUE){
		return true;
	}else{
		return false;
	}

}

//******************************************
// 関数名   ：IsSamplengColorThreshold
// 関数概要 ：サンプリング配列の各要素の値が
//            引数に指定された値の閾値内か判定し、閾値内の割合を判定する
// 引数     ：判定したいカラー（白、黒、灰、走行目標値）
// 戻り値    ：true（閾値内）  false(閾値外)
//******************************************
bool IsSamplengColorThreshold(int checkColor) {
	
	int count = 0;
	
	// 配列の要素分だけ繰り返す
	for (int i = 0; i < SAMPLING_ELEMENT_NUM; i++) {
		
		// 要素の値が閾値内か判定する
		// 引数で指定されたカラー値 - 15 から 引数で指定されたカラー値 + 15に入っていたら
		// カウントを1増やす
		if(checkColor - 5 <= samplengArray[i] && samplengArray[i] <= checkColor + 5){
			count++;
		}
	}
	// カウントが指定された値より多い場合
	if(count >= 8){
		return true;
	}
		
	// カウントが指定された値より小さい場合
	else{
		return false;
	}
}
