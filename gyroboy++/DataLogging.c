#include "ev3api.h"
#include "app.h"
#include "DataLogging.h"


FILE *fp;

//******************************************
// 関数名   ：OpenFile
// 関数概要 ：データロギング用ファイルを開く
// 引数     ：無し
// 戻り値    ：無し
//******************************************
void OpenFile()
{
	if((fp = fopen("log.csv", "w")) != NULL)
	{
		// ヘッダー書き込み
		fprintf(fp,"turn,light,black,white,gray,leftMotor,rightMotor,rate,angle\n");
	}	
}


//******************************************
// 関数名   ：CloseFile
// 関数概要 ：データロギング用ファイルを閉じる
// 引数     ：無し
// 戻り値    ：無し
//******************************************
void CloseFile()
{
    fclose(fp);
}

//******************************************
// 関数名   ：WriteData
// 関数概要 ：ロギング内容をファイルに書き出す
// 引数     ：turn（旋回値）
// 引数     ：light（カラーセンサー）
// 引数     ：black（黒）
// 引数     ：white（白）
// 引数     ：gray（灰）
// 引数		：leftMotor (左モーター)
// 引数		：rightMotor (右モーター)
// 角位置
// 角速度
// 戻り値    ：無し
//******************************************
void WriteData(float turn, int light, int black, int white, int gray,int steer ,int drive)
{
	float leftMotor = ev3_motor_get_counts(EV3_PORT_A);
	float rightMotor = ev3_motor_get_counts(EV3_PORT_D);
	int rate = ev3_gyro_sensor_get_rate(EV3_PORT_2);
	int angle = ev3_gyro_sensor_get_angle(EV3_PORT_2);

	fprintf(fp,"%f,%d,%d,%d,%d,%f,%f,%d,%d,%d,%d\n", turn, light, black, white, gray, leftMotor, rightMotor,rate,angle,steer,drive);
}

